@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <p><a class="btn btn-outline-primary btn-lg" href="{{ route('task.create') }}" role="button">New
                                Task</a></p>
                        <p>
                        <div class="form-group">
                            <label for="project">Project</label>
                            <select class="form-control" id="project" name="project">
                                <option value="#">Select</option>
                                <option value="0">All</option>
                                @if(count($projects) > 0)
                                    @foreach($projects as $project)
                                        <option value="{{ $project->id }}">{{ $project->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        </p>

                        <table id="example" class="table table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>id</th>
                                <th>Name</th>
                                <th>Priority</th>
                                <th>Project</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <!--<tfoot>
                            <tr>
                                <th>id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                            </tr>
                            </tfoot>-->
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page-style')
    <style>

    </style>
@endsection

@section('page-script')
    <script type="text/javascript">
        $(document).ready(function () {
            var t = $('#example').DataTable({
                initComplete: function () {
                    $('#project').on('change', function () {
                        var searchText = $("#project option:selected").text() == "All" ? "" : $("#project option:selected").text();
                        t.search(searchText).draw();
                    });
                },

                //order: [[ 3, 'asc' ]],
                order: [],
                stripeClasses: ['alert-info', 'alert-light'],
                processing: true,
                serverSide: true,
                rowReorder:
                    {
                        snapX: 10,
                        targets: 0
                    },
                ajax: '{{ route('index.data') }}',
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'priority', name: 'priority'},
                    {data: 'projectname', name: 'projectname', defaultContent: ''},
                    {data: 'action', name: 'action', 'orderable': false, 'searchable': false},
                ],
                columnDefs: [
                    {
                        width: 100,
                        targets: 5
                    },
                    {
                        className: 'reorder',
                        targets: [0]
                    }
                ],
            });

            t.on('order.dt search.dt', function () {
                t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();

            t.on('row-reorder', function (e, diff, edit) {
                var data = [];
                for (var i = 0, ien = diff.length; i < ien; i++) {
                    var rowData = t.row(diff[i].node).data();
                    var payload = new Object();
                    payload.id = rowData.id;
                    payload.priority = (+diff[i].newPosition + 1);
                    data.push(payload);
                }

                //console.log(data);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: "{{ route('task.sort') }}",
                    data: {data: data},
                    success: function (data) {
                        if (data.status == 'success') {
                            reload();
                            $.notify(data.message, {
                                type: 'success',
                                newest_on_top: true,
                                animate: {
                                    enter: 'animated bounceIn',
                                    exit: 'animated bounceOut'
                                },
                                //onClosed: reload,
                                offset: 20,
                                spacing: 10,
                                z_index: 1031,
                                delay: 300,
                                timer: 1000,
                                placement: {
                                    align: 'center'
                                },
                            });
                        } else {
                            $.notify(data.message, {
                                type: 'danger',
                                newest_on_top: true,
                                animate: {
                                    enter: 'animated bounceIn',
                                    exit: 'animated bounceOut'
                                },
                                offset: 20,
                                spacing: 10,
                                z_index: 1031,
                                delay: 300,
                                timer: 1000,
                                placement: {
                                    align: 'center'
                                },
                            });
                        }
                    },
                    error: function () {
                        $.notify("An error occured.", {
                            type: 'danger',
                            newest_on_top: true,
                            animate: {
                                enter: 'animated bounceIn',
                                exit: 'animated bounceOut'
                            },
                            offset: 20,
                            spacing: 10,
                            z_index: 1031,
                            delay: 300,
                            timer: 1000,
                            placement: {
                                align: 'center'
                            },
                        });
                    }
                });

            });

            t.on('click', '.btn.btn-danger.btn-sm', function () {
                var buttonVal = $(this).attr('id');
                var url = '{{ route("task.destroy", ["task" => ":id"]) }}';
                url = url.replace(':id', buttonVal);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'DELETE',
                    url: url,
                    //data:{id:$(this).val()},
                    success: function (data) {
                        if (data.status == 'success') {
                            $.notify(data.message, {
                                type: 'success',
                                newest_on_top: true,
                                animate: {
                                    enter: 'animated bounceIn',
                                    exit: 'animated bounceOut'
                                },
                                onClosed: reload,
                                offset: 20,
                                spacing: 10,
                                z_index: 1031,
                                delay: 300,
                                timer: 1000,
                                placement: {
                                    align: 'center'
                                },
                            });
                        } else {
                            $.notify(data.message, {
                                type: 'danger',
                                newest_on_top: true,
                                animate: {
                                    enter: 'animated bounceIn',
                                    exit: 'animated bounceOut'
                                },
                                offset: 20,
                                spacing: 10,
                                z_index: 1031,
                                delay: 300,
                                timer: 1000,
                                placement: {
                                    align: 'center'
                                },
                            });
                        }
                    },
                    error: function () {
                        $.notify("An error occured.", {
                            type: 'danger',
                            newest_on_top: true,
                            animate: {
                                enter: 'animated bounceIn',
                                exit: 'animated bounceOut'
                            },
                            offset: 20,
                            spacing: 10,
                            z_index: 1031,
                            delay: 300,
                            timer: 1000,
                            placement: {
                                align: 'center'
                            },
                        });
                    }
                });

            });

            /*$('#project').on('change', function() {
                alert('değişsti');
            });*/

            function reload() {
                //t.ajax.url( 'url-to-updated-json-data' ).load();
                t.ajax.reload();
            }
        });
    </script>
@stop
