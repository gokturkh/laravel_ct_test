@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form method="POST" action="{{ route('task.store') }}">
                            @csrf
                            <div class="form-group">
                                <label for="taskName">Task Name</label>
                                <input type="text" class="form-control" id="taskName" name="taskName"
                                       aria-describedby="taskNameHelp">
                                <small id="taskNameHelp" class="form-text text-muted">Name of task.</small>
                            </div>
                            <div class="form-group">
                                <label for="priority">Priority</label>
                                <input type="text" class="form-control" id="priority" name="priority"
                                       aria-describedby="priorityHelp">
                                <small id="priorityHelp" class="form-text text-muted">Priority.</small>
                            </div>
                            <div class="form-group">
                                <label for="project">Project</label>
                                <select class="form-control" id="project" name="project">
                                    <option value="0">None</option>
                                    @if(count($projects) > 0)
                                        @foreach($projects as $project)
                                            <option value="{{ $project->id }}">{{ $project->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <small id="projectHelp" class="form-text text-muted">Project.</small>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
