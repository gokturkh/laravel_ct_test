<?php

use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 5; $i++)
        {
            $name = 'Project-' . $i;
            DB::table('projects')->insert([
                'name' => $name
            ]);
        }
    }
}
