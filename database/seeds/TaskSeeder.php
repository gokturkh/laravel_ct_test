<?php

use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 12; $i++)
        {
            $name = 'Task-' . $i;
            $id = mt_rand(1, 5);

            DB::table('tasks')->insert([
                'name' => $name,
                'priority' => 0,
                'project_id' => $id > 3 ? null : $id,
            ]);
        }
    }
}
