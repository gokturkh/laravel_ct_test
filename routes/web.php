<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/', 'IndexController@index')->name('index');
    Route::get('/getData', 'IndexController@getData')->name('index.data');

    Route::resource('task', 'TaskController');
    Route::post('/task/sort', 'TaskController@postSort')->name('task.sort');
});
