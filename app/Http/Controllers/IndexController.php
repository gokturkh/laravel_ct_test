<?php

namespace App\Http\Controllers;

use App\Project;
use App\Task;
use Yajra\DataTables\DataTables;

/**
 * Class IndexController
 * @package App\Http\Controllers
 */
class IndexController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $projects = Project::select('id', 'name')->get();

        return view('index', ['projects' => $projects]);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getData()
    {
        $tasks = Task::selectRaw('tasks.id as "id", tasks.name as "name", tasks.priority as "priority", IFNULL(projects.name, "") as "projectname", tasks.updated_at as "updated_at"')
            ->leftJoin('projects', function ($join) {
                $join->on('tasks.project_id', 'projects.id');
            })
            ->orderBy('tasks.priority', 'asc')
            ->orderBy('tasks.updated_at', 'desc')
            ->get();

        return Datatables::of($tasks)
            ->addIndexColumn()
            ->addColumn('action', function ($tasks) {
                //return '<a class="btn btn-primary btn-sm" href="task/' . $tasks->id . '/edit" role="button">Edit</a><a class="btn btn-danger btn-sm" href="task/' . $tasks->id . '" role="button" id="delete_item" name="delete_item">Delete</a>';
                return '<a class="btn btn-primary btn-sm" href="task/'.$tasks->id.'/edit" role="button">Edit</a><button type="button" id="'.$tasks->id.'" class="btn btn-danger btn-sm">Delete</button>';
            })
            ->make(true);
    }
}
