<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Project;
use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projects = Project::select('id', 'name')->get();

        return view('task.create', ['projects' => $projects]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->isMethod('post')) {
            $validatedData = $request->validate([
                'taskName' => 'required|max:255',
                'priority' => 'required|integer',
                'project' => 'integer',
            ]);

            $newTask = new Task;
            $newTask->name = $request->post('taskName');
            $newTask->priority = intval($request->post('priority'));

            if (intval($request->post('project')) > 0) {
                $newTask->project_id = intval($request->post('project'));
            }

            $saved = $newTask->save();

            if (!$saved) {
                return redirect()->back()->withErrors('Unable to save.');
            }
        }

        Helper::reorderTasks();

        return redirect()->route('index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find(intval($id));
        $projects = Project::select('id', 'name')->get();

        return view('task.edit', ['task' => $task, 'projects' => $projects]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'taskName' => 'required|max:255',
            'priority' => 'required|integer',
            'project' => 'integer',
        ]);

        //if(!$request->isMethod('PUT') || !$request->isMethod('PATCH'))
        if (!$request->isMethod('PUT')) {
            return response()->json(array('status' => 'error', 'message' => 'Wrong method.'));
        }

        $task = Task::find(intval($id));
        if (!$task) {
            return response()->json(array('status' => 'error', 'message' => 'Task not found.'));
        }
        // taskName=Task1&priority=3&project=0

        $task->name = $request->post('taskName');
        $task->priority = intval($request->post('priority'));
        $task->project_id = intval($request->post('project')) === 0 ? null : intval($request->post('project'));

        $saved = $task->save();

        if (!$saved) {
            return redirect()->back()->withErrors('Unable to save.');
        }

        Helper::reorderTasks();

        return redirect()->route('index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (!$request->ajax()) {
            return response()->json(array('status' => 'error', 'message' => 'Unable to delete task.'));
        }

        if (!$request->isMethod('delete')) {
            return response()->json(array('status' => 'error', 'message' => 'Unable to delete task.'));
        }

        $task = Task::find(intval($id));
        if (!$task) {
            return response()->json(array('status' => 'error', 'message' => 'Task not found.'));
        }

        $deleted = $task->delete();
        if (!$deleted) {
            return response()->json(array('status' => 'error', 'message' => 'Unable to delete task.'));
        }

        return response()->json(array('status' => 'success', 'message' => 'Task deleted.'));
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postSort(Request $request)
    {
        if ($request->ajax()) {
            if ($request->isMethod('post')) {
                $items = $request->post('data');

                if (!is_array($items)) {
                    return response()->json(['status' => 'error', 'message' => 'Error no#1.']);
                }

                foreach ($items as $item => $value) {
                    $task = Task::find(intval($value["id"]));

                    if (!$task) {
                        return response()->json(['status' => 'error', 'message' => 'Task not found.']);
                    }

                    $task->priority = intval($value["priority"]);
                    $saved = $task->save();

                    if (!$saved) {
                        return response()->json(['status' => 'error', 'message' => 'Unable to save.']);
                    }
                }
                Helper::reorderTasks();
                return response()->json(['status' => 'success', 'message' => 'Updated.']);
            }
        }
    }
}
