<?php

namespace App\Helpers;

use App\Task;

/**
 * Class Helper
 * @package App\Helpers
 */
class Helper
{
    /**
     *
     */
    public static function reorderTasks()
    {
        $tasks = Task::orderBy('tasks.priority', 'asc')
            ->orderBy('tasks.updated_at', 'desc')
            ->get();

        for ($i = 0; $i < count($tasks); $i++) {
            $tasks[$i]->priority = intval($i + 1);
            $tasks[$i]->save();
        }
    }
}
