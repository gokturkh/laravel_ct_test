<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class ClearCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'all:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear all cache';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Clearing cache");
        try {
            Artisan::call('cache:clear');
        } catch (\Exception $e) {
            $this->error("Error: ".$e->getMessage());
        }
        $this->info("Cache cleared");

        $this->info("\n");

        $this->info("Clearing routes");
        try {
            Artisan::call('route:clear');
        } catch (\Exception $e) {
            $this->error("Error: ".$e->getMessage());
        }
        $this->info("Routes cleared");

        $this->info("\n");

        $this->info("Clearing config");
        try {
            Artisan::call('config:clear');
        } catch (\Exception $e) {
            $this->error("Error: ".$e->getMessage());
        }
        $this->info("Config cleared");

        $this->info("\n");

        $this->info("Clearing views");
        try {
            Artisan::call('view:clear');
        } catch (\Exception $e) {
            $this->error("Error: ".$e->getMessage());
        }
        $this->info("Views cleared");

        $this->info("\n");
        $this->info("Complete");
    }
}
